#!/usr/bin/env sh
echo 'Deploy before testing'
ps aux | grep test-1.0-SNAPSHOT-jar-with-dependencies.jar | awk '{print $2}' | xargs kill -9 || true
cd /home/jenkins/workspace/maven-mockup-build/
mysql -u mockup -phellomoco mockup_db < mockup_db.sql
java -jar target/test-1.0-SNAPSHOT-jar-with-dependencies.jar &
cd testcase/
mvn test "-Dtest=Test.Runner"
